# Congcache

#### 介绍
此项目名称来源：聪

纯java编写分布式缓存,目的只有一个就是为了学习！

现有以下几个工作需要做：
1.添加注释。让更多的人学习编程思路，手法。
2.操作手册未写。
3.没有经常严格测试无法应用于服务端。
4.和spring整合。

在项目的wiki对项目中用到技术点会进行简单介绍。

欢迎加入qq交流群: 659772197

注: 此项目有很多地方借鉴别人之后,此是用于学习.并非商用.如果不妥之处请联系我.


#### 软件架构

此图为项目缓存核心架构图
![架构总图](https://images.gitee.com/uploads/images/2019/0809/171617_de8ca7be_1523748.png "架构总图.png")

项目采用spi操作方式实现，缓存使用双向链表实现缓存，持久化，分布式使用的是插件。只需要在xml中配置即可。



#### 安装教程

无

#### 使用说明

跑一个test1 main方法

#### 参与贡献
欢迎志同道合的同学。


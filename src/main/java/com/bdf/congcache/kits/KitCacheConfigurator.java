package com.bdf.congcache.kits;

import com.bdf.congcache.core.logger.ICacheEventWrapper;
import com.bdf.congcache.core.model.IElementSerializer;
import com.bdf.congcache.utils.config.OptionConverter;
import com.bdf.congcache.utils.config.PropertySetter;
import com.bdf.congcache.utils.serialization.StandardSerializer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Properties;

public class KitCacheConfigurator
{
	private static final Log log = LogFactory.getLog(KitCacheConfigurator.class);

	public static final String ATTRIBUTE_PREFIX = ".attributes";

	public static final String CACHE_EVENT_LOGGER_PREFIX = ".cacheeventlogger";

	public static final String SERIALIZER_PREFIX = ".serializer";

	/**
	 * @description:  创建缓存事件日志
	 * @author: 田培融
	 * @date: 2019/7/23 19:24
	  * @param props	配置文件
	 * @param kitPrefix	 kit前缀
	 * @return: com.bdf.congcache.core.logger.ICacheEventWrapper
	 */
	public static ICacheEventWrapper parseCacheEventLogger(Properties props, String kitPrefix)
	{
		ICacheEventWrapper cacheEventWrapper = null;

		String eventLoggerClassName = kitPrefix + CACHE_EVENT_LOGGER_PREFIX;
		cacheEventWrapper = OptionConverter.instantiateByKey(props, eventLoggerClassName, null);
		if (cacheEventWrapper != null)
		{
			String cacheEventLoggerAttributePrefix = kitPrefix + CACHE_EVENT_LOGGER_PREFIX + ATTRIBUTE_PREFIX;
			PropertySetter.setProperties(cacheEventWrapper, props, cacheEventLoggerAttributePrefix + ".");
			if (log.isInfoEnabled())
			{
				log.info("Use custom cache event logger [" + cacheEventWrapper + "] for kit [" + kitPrefix + "]");
			}
		}
		else
		{
			if (log.isInfoEnabled())
			{
				log.info("No cache event logger defined for kit [" + kitPrefix + "]");
			}
		}
		return cacheEventWrapper;
	}

	public static IElementSerializer parseElementSerializer(Properties props, String kitPrefix)
	{
		IElementSerializer elementSerializer = null;

		String elementSerializerClassName = kitPrefix + SERIALIZER_PREFIX;
		elementSerializer = OptionConverter.instantiateByKey(props, elementSerializerClassName, null);
		if (elementSerializer != null)
		{
			String attributePrefix = kitPrefix + SERIALIZER_PREFIX + ATTRIBUTE_PREFIX;
			PropertySetter.setProperties(elementSerializer, props, attributePrefix + ".");
			if (log.isInfoEnabled())
			{
				log.info("Use custom element serializer [" + elementSerializer + "] for kit [" + kitPrefix + "]");
			}
		}
		else
		{
			elementSerializer = new StandardSerializer();
			if (log.isInfoEnabled())
			{
				log.info("Use standard serializer [" + elementSerializer + "] for kit [" + kitPrefix + "]");
			}
		}
		return elementSerializer;
	}
}

package com.bdf.congcache.kits;


import com.bdf.congcache.core.logger.ICacheEventWrapper;
import com.bdf.congcache.core.model.ICache;
import com.bdf.congcache.core.model.IElementSerializer;
import com.bdf.congcache.core.status.IStats;

import java.io.IOException;
import java.util.Set;

/**
 * 缓存插件接口
 */

public interface KitCache<K, V> extends ICache<K, V>
{
	Set<K> getKeySet() throws IOException;

	IStats getStatistics();

	KitCacheAttributes getKitCacheAttributes();

	void setElementSerializer(IElementSerializer elementSerializer);

	void setCacheEventLogger(ICacheEventWrapper cacheEventWrapper);

}

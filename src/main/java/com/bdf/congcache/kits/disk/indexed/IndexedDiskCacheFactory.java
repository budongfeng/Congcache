package com.bdf.congcache.kits.disk.indexed;

import com.bdf.congcache.core.logger.ICacheEventWrapper;
import com.bdf.congcache.core.model.IContextCacheManager;
import com.bdf.congcache.core.model.IElementSerializer;
import com.bdf.congcache.kits.AbstractKitCacheFactory;
import com.bdf.congcache.kits.KitCacheAttributes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class IndexedDiskCacheFactory extends AbstractKitCacheFactory
{
	private static final Log log = LogFactory.getLog(IndexedDiskCacheFactory.class);

	@Override
	public <K, V> IndexedDiskCache<K, V> createCache(KitCacheAttributes cattr, IContextCacheManager cacheMgr,
													 ICacheEventWrapper cacheEventWrapper, IElementSerializer elementSerializer)
	{
		IndexedDiskCacheAttributes idcattr = (IndexedDiskCacheAttributes) cattr;

		if (log.isDebugEnabled())
		{
			log.debug("Create DiskCache for attributes = " + idcattr);
		}

		IndexedDiskCache<K, V> cache = new IndexedDiskCache<K, V>(idcattr, elementSerializer);

		cache.setCacheEventLogger(cacheEventWrapper);

		return cache;
	}
}

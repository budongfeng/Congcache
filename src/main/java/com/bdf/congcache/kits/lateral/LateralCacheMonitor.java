package com.bdf.congcache.kits.lateral;

import com.bdf.congcache.core.CacheStatus;
import com.bdf.congcache.core.DaemonCacheServiceRemote;
import com.bdf.congcache.core.model.ICacheServiceRemote;
import com.bdf.congcache.kits.AbstractKitCacheMonitor;
import com.bdf.congcache.kits.lateral.tcp.ITCPLateralCacheAttributes;
import com.bdf.congcache.kits.lateral.tcp.LateralTCPCacheFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LateralCacheMonitor extends AbstractKitCacheMonitor
{
	private ConcurrentHashMap<String, LateralCacheAsync<?, ?>> caches;

	private LateralTCPCacheFactory factory;

	protected static void forceShortIdlePeriod(long idlePeriod)
	{
		LateralCacheMonitor.idlePeriod = idlePeriod;
	}

	public LateralCacheMonitor(LateralTCPCacheFactory factory)
	{
		super("CongCacheLateralCacheMonitor");
		this.factory = factory;
		this.caches = new ConcurrentHashMap<String, LateralCacheAsync<?, ?>>();
		setIdlePeriod(20000L);
	}

	public void addCache(LateralCacheAsync<?, ?> cache)
	{
		this.caches.put(cache.getCacheName(), cache);

		if (this.getState() == Thread.State.NEW)
		{
			this.start();
		}
	}

	@Override
	public void dispose()
	{
		this.caches.clear();
	}

	@Override
	public void doWork()
	{

		for (Map.Entry<String, LateralCacheAsync<?, ?>> entry : caches.entrySet())
		{
			String cacheName = entry.getKey();

			@SuppressWarnings("unchecked")
			LateralCacheAsync<Object, Object> cache = (LateralCacheAsync<Object, Object>) entry.getValue();
			if (cache.getStatus() == CacheStatus.ERROR)
			{
				log.info("Find LateralCacheAsync in error, " + cacheName);

				ITCPLateralCacheAttributes attr = (ITCPLateralCacheAttributes) cache.getKitCacheAttributes();

				ICacheServiceRemote<Object, Object> cacheService = factory.getCacheServiceRemoteInstance(attr);

				if (cacheService instanceof DaemonCacheServiceRemote)
				{
					continue;
				}

				cache.fixCache(cacheService);
			}
		}
	}
}

package com.bdf.congcache.kits.lateral;


import com.bdf.congcache.core.model.ICacheListener;
import com.bdf.congcache.core.model.IContextCacheManager;

public interface ILateralCacheListener<K, V> extends ICacheListener<K, V>
{
	void init();

	void setCacheManager(IContextCacheManager cacheMgr);

	IContextCacheManager getCacheManager();

	void dispose();
}

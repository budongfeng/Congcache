package com.bdf.congcache.kits.lateral;

public enum LateralCommand
{
	UPDATE,

	REMOVE,

	REMOVEALL,

	DISPOSE,

	GET,

	GET_MATCHING,

	GET_KEYSET
}

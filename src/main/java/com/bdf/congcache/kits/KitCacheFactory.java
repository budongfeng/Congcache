package com.bdf.congcache.kits;


import com.bdf.congcache.core.logger.ICacheEventWrapper;
import com.bdf.congcache.core.model.IContextCacheManager;
import com.bdf.congcache.core.model.IElementSerializer;

/**
 * 缓存插件工厂类
 */
public interface KitCacheFactory
{
	<K, V> KitCache<K, V> createCache(KitCacheAttributes attr, IContextCacheManager cacheMgr,
									  ICacheEventWrapper cacheEventWrapper, IElementSerializer elementSerializer) throws Exception;

	void initialize();

	void dispose();

	void setName(String s);

	String getName();
}

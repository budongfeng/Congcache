package com.bdf.congcache.xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

public class XmlParser
{

	private static final Log log = LogFactory.getLog(XmlParser.class);

	/**
	 * @description:  从xml配置文件，将xml文件转换配置文件对象
	 * @author: 田培融
	 * @date: 2019/7/20 19:08
	  * @param xmlFile 配置文件
	 * @return: java.util.Properties
	 */
	public static Properties getPropertiesFromXml(String xmlFile)
	{
		InputStream is = XmlParser.class.getResourceAsStream(xmlFile);

		XPathParser parser = new XPathParser(is);

		// “/configuration” xml文件根配置文件
		XNode root = parser.evalNode("/configuration");

		XNode caches = root.evalNode("caches");
		Properties prop = new Properties();
		for (XNode cache : caches.getChildren())
		{

			String id = cache.getStringAttribute("id");

			String prefix = "CongCache." + id;

			XNode cacheKit = cache.evalNode("kit");

			String kitStr = cacheKit.getStringBody();

			if (kitStr == null || kitStr.isEmpty())
			{
				prop.setProperty(prefix, "");
			}
			else
			{
				prop.setProperty(prefix, kitStr);
			}

			XNode cacheattributes = cache.evalNode("cacheattributes");

			String cls = cacheattributes.getStringAttribute("class");

			prop.setProperty(prefix + ".cacheattributes", cls);

			Properties properties = cacheattributes.getChildrenAsProperties();
			addPrefix(properties, prefix + ".cacheattributes", prop);

			XNode elementattributes = cache.evalNode("elementattributes");
			cls = elementattributes.getStringAttribute("class");
			prop.setProperty(prefix + ".elementattributes", cls);

			Properties elementProp = elementattributes.getChildrenAsProperties();
			addPrefix(elementProp, prefix + ".elementattributes", prop);
		}

		XNode kits = root.evalNode("kits");

		for (XNode kit : kits.getChildren())
		{
			String id = kit.getStringAttribute("id");
			String fact = kit.getStringAttribute("class");

			XNode attributes = kit.evalNode("attributes");
			String cls = attributes.getStringAttribute("class");

			prop.setProperty("CongCache.kit." + id, fact);
			prop.setProperty("CongCache.kit." + id + ".attributes", cls);

			Properties properties = attributes.getChildrenAsProperties();
			addPrefix(properties, "CongCache.kit." + id + ".attributes", prop);

		}

		xml2PropertyLogger(prop);
		return prop;
	}

	private static void addPrefix(Properties originalPro, String prefix, Properties currentPro)
	{
		Iterator<?> it = originalPro.entrySet().iterator();
		while (it.hasNext())
		{
			@SuppressWarnings("unchecked")
			Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
			String key = entry.getKey();
			String value = entry.getValue();
			currentPro.setProperty(prefix + "." + key, value);
		}
	}

	private static void xml2PropertyLogger(Properties properties)
	{
		Iterator<?> it = properties.entrySet().iterator();
		while (it.hasNext())
		{
			@SuppressWarnings("unchecked")
			Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
			String key = entry.getKey();
			String value = entry.getValue();
			log.info(key + "=" + value);
		}
	}
}

package com.bdf.congcache.utils.serialization;


import com.bdf.congcache.core.model.IElementSerializer;
import com.bdf.congcache.io.IOClassLoaderWarpper;

import java.io.*;

/**
 * @description: 标准序列化操作磁盘
 * @author: 田培融
 * @date: 2019/7/23 19:28
 */
public class StandardSerializer implements IElementSerializer
{
	@Override
	public <T> byte[] serialize(T obj) throws IOException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		try
		{
			oos.writeObject(obj);
		}
		finally
		{
			oos.close();
		}
		return baos.toByteArray();
	}

	@Override
	public <T> T deSerialize(byte[] data, ClassLoader loader) throws IOException, ClassNotFoundException
	{
		ByteArrayInputStream bais = new ByteArrayInputStream(data);
		BufferedInputStream bis = new BufferedInputStream(bais);
		ObjectInputStream ois = new IOClassLoaderWarpper(bis, loader);
		try
		{
			@SuppressWarnings("unchecked")
			T readObject = (T) ois.readObject();
			return readObject;
		}
		finally
		{
			ois.close();
		}
	}
}

package com.bdf.congcache.utils.struct;

import java.io.Serializable;

/**
 * @Author 田培融
 * @Description  双向链表人结点 ，  此处用于保存缓存值  ICacheElement
 * @Date 10:52 2019/7/12
 **/
public class DoubleLinkedListNode<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    // T 为节点中保存的值，可以为任意值
    private final T payload;

    // 节点的前指针
    public DoubleLinkedListNode<T> prev;

    // 节点的后指针
    public DoubleLinkedListNode<T> next;

    public DoubleLinkedListNode(T payload)
    {
        this.payload = payload;
    }


    public T getPayload()
    {
        return this.payload;
    }
}

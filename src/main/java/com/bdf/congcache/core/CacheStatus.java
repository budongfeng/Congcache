package com.bdf.congcache.core;

public enum CacheStatus
{
	ALIVE,

	DISPOSED,

	ERROR
}

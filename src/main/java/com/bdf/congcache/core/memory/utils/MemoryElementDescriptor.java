package com.bdf.congcache.core.memory.utils;

import com.bdf.congcache.core.model.ICacheElement;
import com.bdf.congcache.utils.struct.DoubleLinkedListNode;

//文件系统描述
public class MemoryElementDescriptor<K,V> extends DoubleLinkedListNode<ICacheElement<K,V>>{

    private static final long serialVersionUID = 1L;
    /**
     * @description: 构建链表节点
     * @author: 田培融
     * @date: 2019/7/31 9:13
      * @param payload 链表节点的值， 此处为缓存元素
     * @return:
     */
    public MemoryElementDescriptor(ICacheElement<K, V> payload) {
        super(payload);
    }

    public ICacheElement<K, V> getCacheElement()
    {
        return getPayload();
    }
}

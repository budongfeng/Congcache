package com.bdf.congcache.core.memory;

import com.bdf.congcache.core.control.ContextCache;
import com.bdf.congcache.core.model.ICacheElement;
import com.bdf.congcache.core.model.IContextCacheAttributes;
import com.bdf.congcache.core.status.IStats;

import javax.naming.Context;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * @Author 田培融
 * @Description 内存缓存接口
 * @Date 8:54 2019/7/11
 **/
public interface IMemoryCache<K,V> {

    //生命周期：开始
    void initialize(ContextCache<K, V> cache);

    //生命周期：结束
    void dispose() throws IOException;

    //统计相关
    int getSize();

    Set<K> getKeySet();

    //读写操作
    ICacheElement<K, V> get(K key) throws IOException;

    Map<K, ICacheElement<K, V>> getMultiple(Set<K> keys) throws IOException;

    ICacheElement<K, V> getQuiet(K key) throws IOException;

    void update(ICacheElement<K, V> ce) throws IOException;

    void waterfal(ICacheElement<K, V> ce) throws IOException;

    boolean remove(K key) throws IOException;

    void removeAll() throws IOException;

    int freeElements(int numberToFree) throws IOException;

    //属性设置
    IContextCacheAttributes getCacheAttributes();

    void setCacheAttributes(IContextCacheAttributes cattr);

    ContextCache<K, V> getContextCache();

    public IStats getStatistics();


}

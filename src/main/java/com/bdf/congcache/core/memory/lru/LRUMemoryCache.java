package com.bdf.congcache.core.memory.lru;

import com.bdf.congcache.core.AbstractDoubleLinkedListMemoryCache;
import com.bdf.congcache.core.memory.utils.MemoryElementDescriptor;
import com.bdf.congcache.core.model.ICacheElement;

import java.io.IOException;

/**
 * @description: 缓存链表特性操作类
 * @author: 田培融
 * @date: 2019/7/26 7:29
 */
public class LRUMemoryCache<K,V>  extends AbstractDoubleLinkedListMemoryCache<K,V> {


    /**
     * @description: 在双向链表的第一位置添加元素
     * @author: 田培融
     * @date: 2019/7/31 9:04
      * @param ce 元素值
     * @return: com.bdf.congcache.core.memory.utils.MemoryElementDescriptor<K,V>
     */
    @Override
    protected MemoryElementDescriptor<K,V> adjustListForUpdate(ICacheElement<K, V> ce) throws IOException {
        return addFirst(ce);
    }

    @Override
    protected void adjustListForGet(MemoryElementDescriptor<K, V> me) {
        // 缓存链表
        list.makeFirst(me);
    }


}

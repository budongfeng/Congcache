package com.bdf.congcache.core;

public interface CacheConstants
{
	// 配置文件的名称
	String DEFAULT_CONFIG = "/CongCache.xml";

	String NAME_COMPONENT_DELIMITER = ":";
}

package com.bdf.congcache.core.control.event;

import java.util.EventObject;

/**
 * @description:
 * @author: 田培融  事件状态对象， 用于监听对象中的参数
 * @date: 2019/7/21 13:49
 */
public class ElementEvent<T> extends EventObject implements IElementEvent<T>
{
	private static final long serialVersionUID = 1L;

	private ElementEventType elementEvent = ElementEventType.EXCEEDED_MAXLIFE_BACKGROUND;

	/**
	 * @description:  事件构造器
	 * @author: 田培融
	 * @date: 2019/7/21 15:16
	  * @param source	事件源 可以是任意类
	 * @param elementEvent	事件类型
	 */
	public ElementEvent(T source, ElementEventType elementEvent)
	{
		super(source);
		this.elementEvent = elementEvent;
	}

	@Override
	public ElementEventType getElementEvent()
	{
		return elementEvent;
	}

	/**
	 * @description:  获取事件源
	 * @author: 田培融
	 * @date: 2019/7/21 15:16
	 * @return: T
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T getSource()
	{
		return (T) super.getSource();

	}
}

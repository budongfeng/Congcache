package com.bdf.congcache.core.control.event;

/**
 * @Author 田培融
 * @Description  事件处理器接口
 * @Date 15:05 2019/7/10
 **/
public interface IElementEventHandler {
    <T> void handleElementEvent(IElementEvent<T> elementEvent);
}

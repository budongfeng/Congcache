package com.bdf.congcache.core.control.event;

import java.io.Serializable;

/**
 * @Author 田培融
 * @Description 缓存事件
 * @Date 15:06 2019/7/10
 **/
public interface IElementEvent<T> extends Serializable {

    ElementEventType getElementEvent();

    T getSource();
}

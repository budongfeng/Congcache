package com.bdf.congcache.core.control;

import com.bdf.congcache.core.control.event.IElementEventQueue;
import com.bdf.congcache.core.logger.ICacheEventWrapper;
import com.bdf.congcache.core.model.*;
import com.bdf.congcache.kits.KitCache;
import com.bdf.congcache.kits.KitCacheAttributes;
import com.bdf.congcache.kits.KitCacheConfigurator;
import com.bdf.congcache.kits.KitCacheFactory;
import com.bdf.congcache.utils.config.OptionConverter;
import com.bdf.congcache.utils.config.PropertySetter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;


/**
 * @description: 缓存上下文配置类，通过此类生成缓存上下文对象
 * @author: 田培融
 * @date: 2019/8/6 14:58
 */
public class ContextCacheConfigurator
{
	private static final Log log = LogFactory.getLog(ContextCacheConfigurator.class);

	protected static final String SYSTEM_PROPERTY_KEY_PREFIX = "CongCache";

	protected static final String CACHE_PREFIX = "CongCache.";

	protected static final String SYSTEM_CACHE_PREFIX = "CongCache.system.";

	protected static final String KIT_PREFIX = "CongCache.kit.";

	protected static final String ATTRIBUTE_PREFIX = ".attributes";

	protected static final String CACHE_ATTRIBUTE_PREFIX = ".cacheattributes";

	protected static final String ELEMENT_ATTRIBUTE_PREFIX = ".elementattributes";

	public static final String KEY_MATCHER_PREFIX = ".keymatcher";

	public ContextCacheConfigurator()
	{

	}


	/**
	 * @Author 田培融
	 * @Description 将系统中自带的properties中的值添加到 props对象中。
	 * @Date 8:14 2019/7/12
	 **/
	protected static void overrideWithSystemProperties(Properties props)
	{
		Properties sysProps = System.getProperties();
		for (String key : sysProps.stringPropertyNames())
		{
			if (key.startsWith(SYSTEM_PROPERTY_KEY_PREFIX))
			{
				if (log.isInfoEnabled())
				{
					log.info("Use system property [[" + key + "] [" + sysProps.getProperty(key) + "]]");
				}
				props.setProperty(key, sysProps.getProperty(key));
			}
		}
	}


	protected IContextCacheAttributes parseContextCacheAttributes(Properties props, String cacheName,
																  IContextCacheAttributes defaultCCAttr)
	{
		return parseContextCacheAttributes(props, cacheName, defaultCCAttr, CACHE_PREFIX);
	}


	/**
	 * @Author 田培融
	 * @Description 解析内存空间属性
	 * @Date 8:54 2019/7/12
	 * @Param [props  自定义的properties对象 , cacheName 缓存名称, defaultCCAttr 默认的缓存空间的属性值 , regionPrefix  默契空间的前缀]
	 **/
	protected IContextCacheAttributes parseContextCacheAttributes(Properties props, String cacheName,
																  IContextCacheAttributes defaultCCAttr, String regionPrefix)
	{
		IContextCacheAttributes ccAttr;

		String attrName = regionPrefix + cacheName + CACHE_ATTRIBUTE_PREFIX;

		ccAttr = OptionConverter.instantiateByKey(props, attrName, null);

		if (ccAttr == null)
		{
			if (log.isInfoEnabled())
			{
				log.info("No special ContextCacheAttributes class defined for key [" + attrName
						+ "], using default class.");
			}

			ccAttr = defaultCCAttr;
		}

		if (log.isDebugEnabled())
		{
			log.debug("Parse options for: " + attrName);
		}

		PropertySetter.setProperties(ccAttr, props, attrName + ".");
		ccAttr.setCacheName(cacheName);

		if (log.isDebugEnabled())
		{
			log.debug("End of parsing for: " + attrName);
		}

		ccAttr.setCacheName(cacheName);
		return ccAttr;
	}


	/**
	 * @Author 田培融
	 * @Description 生成元素属性
	 * @Date 10:39 2019/7/15
	 * @Param [props, cacheName, defaultEAttr, regionPrefix]
	 * @return com.bdf.congcache.core.model.IElementAttributes
	 **/
	protected IElementAttributes parseElementAttributes(Properties props, String cacheName,
														IElementAttributes defaultEAttr, String regionPrefix)
	{
		IElementAttributes eAttr;

		String attrName = regionPrefix + cacheName + ContextCacheConfigurator.ELEMENT_ATTRIBUTE_PREFIX;

		eAttr = OptionConverter.instantiateByKey(props, attrName, null);
		if (eAttr == null)
		{
			if (log.isInfoEnabled())
			{
				log.info("No special ElementAttribute class defined for key [" + attrName + "], using default class.");
			}

			eAttr = defaultEAttr;
		}

		if (log.isDebugEnabled())
		{
			log.debug("Parse options for: " + attrName);
		}

		PropertySetter.setProperties(eAttr, props, attrName + ".");

		if (log.isDebugEnabled())
		{
			log.debug("End of parsing for: " + attrName);
		}

		return eAttr;
	}


	/**
	 * @description:
	 * @author: 田培融
	 * @date: 2019/7/22 19:39
	  * @param props 配置文件
	 * @param ccm 缓存上下文管理器
	 * @return: void
	 */
	protected void parseSystemCaches(Properties props, ContextCacheManager ccm)
	{
		for (String key : props.stringPropertyNames())
		{
			if (key.startsWith(SYSTEM_CACHE_PREFIX) && key.indexOf("attributes") == -1)
			{
				String cacheName = key.substring(SYSTEM_CACHE_PREFIX.length());
				String kits = OptionConverter.findAndSubst(key, props);
				ICache<?, ?> cache;
				synchronized (cacheName)
				{
					cache = parseCache(props, ccm, cacheName, kits, null, SYSTEM_CACHE_PREFIX);
				}
				ccm.addCache(cacheName, cache);
			}
		}
	}


	protected <K, V> ContextCache<K, V> parseCache(Properties props, ContextCacheManager ccm, String cacheName,
												   String kits)
	{
		return parseCache(props, ccm, cacheName, kits, null, CACHE_PREFIX);
	}

	/**
	 * @Author 田培融
	 * @Description 创建ContextCache
	 * @Date 10:08 2019/7/15
	 * @Param [props  配置文件, ccm缓存上下文管理器, cacheName缓存上下文名称, kits 默认值, cca]
	 **/
	protected <K, V> ContextCache<K, V> parseCache(Properties props, ContextCacheManager ccm, String cacheName,
												   String kits, IContextCacheAttributes cca)
	{
		return parseCache(props, ccm, cacheName, kits, cca, CACHE_PREFIX);
	}


	/**
	 * @description:  生成缓存上下文
	 * @author: 田培融
	 * @date: 2019/7/23 19:02
	  * @param props	配置文件
	 * @param ccm	缓存上下文管理器
	 * @param cacheName	缓存名称
	 * @param kits	缓存插件
	 * @param cca	缓存上下文属性
	 * @param cachePrefix	缓存前缀
	 * @return: com.bdf.congcache.core.control.ContextCache<K,V>
	 */
	protected <K, V> ContextCache<K, V> parseCache(Properties props, ContextCacheManager ccm, String cacheName,
												   String kits, IContextCacheAttributes cca, String cachePrefix)
	{
		IElementAttributes ea = parseElementAttributes(props, cacheName, ccm.getDefaultElementAttributes(),
				cachePrefix);

		IContextCacheAttributes instantiationCca = cca == null
				? parseContextCacheAttributes(props, cacheName, ccm.getDefaultCacheAttributes(), cachePrefix)
				: cca;

		// 生成缓存，根据配置文件生成，生成双向链表的缓存
		ContextCache<K, V> cache = newCache(instantiationCca, ea);

		// 将上下文管理器添加到缓存上下文中
		cache.setContextCacheManager(ccm);

		//定时压缩内存
		cache.setScheduledExecutorService(ccm.getScheduledExecutorService());

		// 事件队列
		cache.setElementEventQueue(ccm.getElementEventQueue());

		//  是否有需要跑的定时线程
		if (cache.getMemoryCache() instanceof IRequireScheduler)
		{
			((IRequireScheduler) cache.getMemoryCache()).setScheduledExecutorService(ccm.getScheduledExecutorService());
		}

		// 生成缓存组件
		if (kits != null)
		{
			List<KitCache<K, V>> kitList = new ArrayList<KitCache<K, V>>();

			if (log.isDebugEnabled())
			{
				log.debug("Parse cache name '" + cacheName + "', value '" + kits + "'");
			}
			StringTokenizer st = new StringTokenizer(kits, ",");

			if (!(kits.startsWith(",") || kits.equals("")))
			{
				if (!st.hasMoreTokens())
				{
					return null;
				}
			}
			KitCache<K, V> kitCache;
			String kitName;
			while (st.hasMoreTokens())
			{
				kitName = st.nextToken().trim();
				if (kitName == null || kitName.equals(","))
				{
					continue;
				}
				log.debug("Parse kit named: " + kitName);
				// 生成缓存组件
				kitCache = parseKit(props, ccm, kitName, cacheName);
				if (kitCache != null)
				{
					if (kitCache instanceof IRequireScheduler)
					{
						((IRequireScheduler) kitCache).setScheduledExecutorService(ccm.getScheduledExecutorService());
					}
					kitList.add(kitCache);
				}
			}
			@SuppressWarnings("unchecked")
			KitCache<K, V>[] kitArray = kitList.toArray(new KitCache[0]);
			cache.setKitCaches(kitArray);
		}

		return cache;
	}

	protected <K, V> ContextCache<K, V> newCache(IContextCacheAttributes cca, IElementAttributes ea)
	{
		return new ContextCache<K, V>(cca, ea);
	}

	protected <K, V> KitCache<K, V> parseKit(Properties props, ContextCacheManager ccm, String kitName,
											 String cacheName)
	{
		if (log.isDebugEnabled())
		{
			log.debug("Parse kit " + kitName);
		}

		@SuppressWarnings("unchecked")
		KitCache<K, V> kitCache = (KitCache<K, V>) ccm.getKitCache(kitName, cacheName);

		if (kitCache == null)
		{
			KitCacheFactory kitFac = ccm.registryFacGet(kitName);
			if (kitFac == null)
			{
				String prefix = KIT_PREFIX + kitName;
				kitFac = OptionConverter.instantiateByKey(props, prefix, null);
				if (kitFac == null)
				{
					log.error("Could not instantiate kitFactory named: " + kitName);
					return null;
				}

				kitFac.setName(kitName);

				if (kitFac instanceof IRequireScheduler)
				{
					((IRequireScheduler) kitFac).setScheduledExecutorService(ccm.getScheduledExecutorService());
				}

				kitFac.initialize();
				ccm.registryFacPut(kitFac);
			}

			KitCacheAttributes kitAttr = ccm.registryAttrGet(kitName);
			String attrName = KIT_PREFIX + kitName + ATTRIBUTE_PREFIX;
			if (kitAttr == null)
			{
				String prefix = KIT_PREFIX + kitName + ATTRIBUTE_PREFIX;
				kitAttr = OptionConverter.instantiateByKey(props, prefix, null);
				if (kitAttr == null)
				{
					log.error("Could not instantiate kitAttr named '" + attrName + "'");
					return null;
				}
				kitAttr.setName(kitName);
				ccm.registryAttrPut(kitAttr);
			}

			kitAttr = kitAttr.clone();

			if (log.isDebugEnabled())
			{
				log.debug("Parse options for '" + attrName + "'");
			}

			PropertySetter.setProperties(kitAttr, props, attrName + ".");
			kitAttr.setCacheName(cacheName);

			if (log.isDebugEnabled())
			{
				log.debug("End of parsing for '" + attrName + "'");
			}

			kitAttr.setCacheName(cacheName);

			String kitPrefix = KIT_PREFIX + kitName;

			ICacheEventWrapper cacheEventWrapper = KitCacheConfigurator.parseCacheEventLogger(props, kitPrefix);

			IElementSerializer elementSerializer = KitCacheConfigurator.parseElementSerializer(props, kitPrefix);

			try
			{
				kitCache = kitFac.createCache(kitAttr, ccm, cacheEventWrapper, elementSerializer);
			}
			catch (Exception e)
			{
				log.error("Could not instantiate kit cache named: " + cacheName);
				return null;
			}

			ccm.addKitCache(kitName, cacheName, kitCache);
		}

		return kitCache;
	}

	/**
	 * @description: 通过配置文件生成缓存上下文(ContextCache)   并其添加到缓存上下文管理器中。
	 * @author: 田培融
	 * @date: 2019/7/23 17:46
	  * @param props 配置文件
	 * @param ccm	缓存上下文管理器
	 */
	protected void parseCaches(Properties props, ContextCacheManager ccm)
	{
		List<String> cacheNames = new ArrayList<String>();

		for (String key : props.stringPropertyNames())
		{
			if (key.startsWith(CACHE_PREFIX) && key.indexOf("attributes") == -1)
			{
				if (key.startsWith("CongCache.kit"))
				{
					continue;
				}
				String cacheName = key.substring(CACHE_PREFIX.length());
				cacheNames.add(cacheName);
				String kits = OptionConverter.findAndSubst(key, props);
				ICache<?, ?> cache;
				synchronized (cacheName)
				{
					cache = parseCache(props, ccm, cacheName, kits);
				}
				ccm.addCache(cacheName, cache);
			}
		}

		if (log.isInfoEnabled())
		{
			log.info("Parse caches " + cacheNames);
		}
	}

}

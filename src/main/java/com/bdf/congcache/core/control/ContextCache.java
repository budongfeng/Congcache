package com.bdf.congcache.core.control;

import com.bdf.congcache.access.exception.CacheException;
import com.bdf.congcache.access.exception.ObjectNotFoundException;
import com.bdf.congcache.core.CacheStatus;
import com.bdf.congcache.core.control.event.*;
import com.bdf.congcache.core.match.IKeyMatcher;
import com.bdf.congcache.core.match.KeyMatcher;
import com.bdf.congcache.core.memory.IMemoryCache;
import com.bdf.congcache.core.memory.lru.LRUMemoryCache;
import com.bdf.congcache.core.memory.shrinking.ShrinkingThread;
import com.bdf.congcache.core.model.*;
import com.bdf.congcache.core.status.*;
import com.bdf.congcache.kits.KitCache;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.bdf.congcache.core.model.IContextCacheAttributes.DiskUsagePattern;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author 田培融
 * @Description  缓存内容实体包装类，key value 值是在IMemCache中
 * @Date 20:02 2019/7/10
 **/
public class ContextCache<K,V> implements ICache<K,V>, IRequireScheduler {

    private static final Log log = LogFactory.getLog(ContextCache.class);

    private KitCache<K, V>[] kitCaches = new KitCache[0];


    private ContextCacheManager cacheManager = null;


    private IKeyMatcher<K> keyMatcher = new KeyMatcher<K>();

    private ScheduledFuture<?> future;

    // 缓存属性
    private IContextCacheAttributes cacheAttr;

    //缓存元素的属性，在创建缓存元素的时候需要给元素元素添加属性，来源于此处。
    private IElementAttributes attr;

    private AtomicBoolean alive;

    private AtomicInteger updateCount;

    private AtomicInteger removeCount;

    private AtomicInteger hitCountMemCache;

    private AtomicInteger hitCountKitCache;

    private AtomicInteger missCountNotFound;

    private AtomicInteger missCountExpired;

    private IMemoryCache<K,V> memCache;

    private IElementEventQueue elementEventQueue;


    public ContextCache(IContextCacheAttributes cattr, IElementAttributes attr)
    {
        this.attr = attr;
        this.cacheAttr = cattr;
        this.alive = new AtomicBoolean(true);
        this.updateCount = new AtomicInteger(0);
        this.removeCount = new AtomicInteger(0);
        this.hitCountMemCache = new AtomicInteger(0);
        this.hitCountKitCache = new AtomicInteger(0);
        this.missCountNotFound = new AtomicInteger(0);
        this.missCountExpired = new AtomicInteger(0);

        createMemoryCache(cattr);

        if (log.isInfoEnabled())
        {
            log.info("Create cache with name [" + cacheAttr.getCacheName() + "] and cache attributes " + cattr);
        }
    }

    public IElementAttributes getElementAttributes()
    {
        if (attr != null)
        {
            return attr.clone();
        }
        return null;
    }

    public IElementAttributes getElementAttributes(K key) throws CacheException, IOException
    {
        ICacheElement<K, V> ce = get(key);
        if (ce == null)
        {
            throw new ObjectNotFoundException("Key " + key + " is not found");
        }
        return ce.getElementAttributes();
    }

    /**
     * @Author 田培融
     * @Description  创建内存缓存对象
     * @Date 10:08 2019/7/12
     **/
    private void createMemoryCache(IContextCacheAttributes cattr)
    {
        if (memCache == null)
        {
            try
            {
                // 能过反射生成缓存
                Class<?> c = Class.forName(cattr.getMemoryCacheName());
                @SuppressWarnings("unchecked")
                IMemoryCache<K, V> newInstance = (IMemoryCache<K, V>) c.newInstance();
                memCache = newInstance;
                memCache.initialize(this);
            }
            catch (Exception e)
            {
                log.warn("Fail to init mem cache.", e);

                this.memCache = new LRUMemoryCache<K, V>();
                this.memCache.initialize(this);
            }
        }
        else
        {
            log.warn("Memory cache already exists.");
        }
    }

    @Override
    public void setScheduledExecutorService(ScheduledExecutorService scheduledExecutor) {
        //
        if (cacheAttr.isUseMemoryShrinker()){
            // 以固定的频率执行
            // TODO 压缩内存空间
//            scheduledExecutor.scheduleAtFixedRate(new ShrinkingThread<>());
        }
    }




    public IContextCacheAttributes getCacheAttributes()
    {
        return this.cacheAttr;
    }

    public boolean isExpired(ICacheElement<K, V> element)
    {
        return isExpired(element, System.currentTimeMillis(), ElementEventType.EXCEEDED_MAXLIFE_ONREQUEST,
                ElementEventType.EXCEEDED_IDLETIME_ONREQUEST);
    }

    /**
     * @Author 田培融
     * @Description 判断缓存是否过期，如果过期就将期放到队列中
     * @Date 10:10 2019/7/11
     **/
    public boolean isExpired(ICacheElement<K,V> element , long timestamp, ElementEventType eventMaxLife,
                             ElementEventType eventIdle){

        try {
            IElementAttributes attributes = element.getElementAttributes();
            if (!attributes.getIsEternal()){
                final long timeFactorForMilliseconds = attributes.getTimeFactorForMilliseconds();

                // 将过期缓存内容创建成事件 添加到事件队列中
                long maxLifeSeconds = attributes.getMaxLife();
                long createTime = attributes.getCreateTime();
                // 过期时间基数
                if (maxLifeSeconds!=-1&& (timestamp-createTime)>maxLifeSeconds*timeFactorForMilliseconds){
                    if (log.isDebugEnabled()){
                        log.debug("Exceed maxLife : "+ element.getKey());
                    }
                    // 将过期缓存内容创建成事件 添加到事件队列中
                    handleElementEvent(element,eventMaxLife);
                    return true;
                }

                // 如果缓存的空闲时间过长，则将缓存添加到队列中
                long idleTime = attributes.getIdleTime();
                long lastAccessTime = attributes.getLastAccessTime();
                if ((idleTime != -1) && (timestamp - lastAccessTime) > idleTime * timeFactorForMilliseconds)
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("Exceed maxIdle: " + element.getKey());
                    }
                    handleElementEvent(element, eventIdle);
                    return true;
                }
            }
        }catch (Exception e){
            log.error("Handler expired element error",e);
            return true;
        }
        return false;
    }

    /**
     * @Author 田培融
     * @Description 根据事件类型和缓存内容来创建事件，并将其添加到事件队列中。
     * @Date 11:06 2019/7/11
     * @Param [element, eventType]  element 缓存内容，  eventType 事件类型
     **/
    public void handleElementEvent(ICacheElement<K,V> element,ElementEventType eventType){
        ArrayList<IElementEventHandler> eventHandlers = element.getElementAttributes().getElementEventHandlers();

        if (eventHandlers != null){
            if (log.isDebugEnabled()){
                log.debug("create event type "+ eventType);
            }
            if (elementEventQueue == null)
            {
                log.warn("No element event queue available for cache " + getCacheName());
                return;
            }

            IElementEvent<ICacheElement<K, V>> event = new ElementEvent<ICacheElement<K, V>>(element, eventType);
            for (IElementEventHandler handler : eventHandlers){
                try {
                    elementEventQueue.addElementEvent(handler,event);
                }catch (IOException e){
                    log.error("Add element event to queue error.",e);
                }
            }
        }
    }

    /**
     * @Author 田培融
     * @Description 将数据刷新到磁盘中，注： 当缓存数量过大时
     * @Date 14:19 2019/7/12
     **/
    public void spoolToDisk(ICacheElement<K, V> ce)
    {

        // 从内容中判断是否要刷新到磁盘
        if (!ce.getElementAttributes().getIsSpool())
        {
            // 将缓存添加到队列中执行
            handleElementEvent(ce, ElementEventType.SPOOLED_NOT_ALLOWED);
            return;
        }

        boolean diskAvailable = false;

        for (ICache<K, V> kitCache : kitCaches)
        {
            if (kitCache != null && kitCache.getCacheType() == CacheType.DISK_CACHE)
            {
                diskAvailable = true;

                if (cacheAttr.getDiskUsagePattern() == DiskUsagePattern.SWAP)
                {
                    try
                    {
                        handleElementEvent(ce, ElementEventType.SPOOLED_DISK_AVAILABLE);
                        kitCache.update(ce);
                    }
                    catch (IOException ex)
                    {
                        log.error("Spool to disk cache error.", ex);
                        throw new IllegalStateException(ex.getMessage());
                    }

                    if (log.isDebugEnabled())
                    {
                        log.debug("Spool to disk  for: " + ce.getKey() + " on disk cache[" + kitCache.getCacheName()
                                + "]");
                    }
                }
                else
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("CacheKit is not configured to use the DiskCache as a swap.");
                    }
                }
            }
        }

        if (!diskAvailable)
        {
            try
            {
                handleElementEvent(ce, ElementEventType.SPOOLED_DISK_NOT_AVAILABLE);
            }
            catch (Exception e)
            {

            }
        }
    }


    /**
     * @Author 田培融
     * @Description 获取缓存上下文的属性对象
     * @Date 18:05 2019/7/19
     **/
    public ICacheStats getStatistics()
    {
        ICacheStats stats = new CacheStats();
        stats.setCacheName(this.getCacheName());

        ArrayList<IStatElement<?>> elems = new ArrayList<IStatElement<?>>();

        elems.add(new StatElement<Integer>("HitCountMemCache", Integer.valueOf(getHitCountMemCache())));
        elems.add(new StatElement<Integer>("HitCountKitCache", Integer.valueOf(getHitCountKitCache())));

        stats.setStatElements(elems);

        int total = kitCaches.length + 1;
        ArrayList<IStats> kitCacheStats = new ArrayList<IStats>(total);

        kitCacheStats.add(getMemoryCache().getStatistics());

        for (KitCache<K, V> kit : kitCaches)
        {
            kitCacheStats.add(kit.getStatistics());
        }

        stats.setKitCacheStats(kitCacheStats);

        return stats;
    }



    /**
     * @Author 田培融
     * @Description  缓存命中的次数
     * @Date 18:07 2019/7/19
     **/
    public int getHitCountMemCache()
    {
        return hitCountMemCache.get();
    }

    /**
     * @Author 田培融
     * @Description 组件缓存的命中次数
     * @Date 18:07 2019/7/19
     **/
    public int getHitCountKitCache()
    {
        return hitCountKitCache.get();
    }


    /**
     * @description: 更新缓存上下文
     * @author: 田培融
     * @date: 2019/7/31 8:40
      * @param ce
     * @return: void
     */
    @Override
    public void update(ICacheElement<K, V> ce) throws IOException
    {
        update(ce, false);
    }


    /**
     * @description: 更新缓存上下文
     * @author: 田培融
     * @date: 2019/7/31 8:44
      * @param cacheElement	缓存元素
     * @param localOnly	是否只更新本地，false： 不只更新本地，也更新分布式的其他服务。 true： 只更新本地
     * @return: void
     */
    protected void update(ICacheElement<K, V> cacheElement, boolean localOnly) throws IOException
    {


        if (log.isDebugEnabled())
        {
            log.debug("Update memory cache " + cacheElement.getKey());
        }

        updateCount.incrementAndGet();

        synchronized (this)
        {
            // 创建链表节点，并将节点添加到链表中
            memCache.update(cacheElement);
            //TODO 组件操作缓存元素， 如：分布式之间通信， 磁盘操作
            updateKits(cacheElement, localOnly);
        }

        cacheElement.getElementAttributes().setLastAccessTimeNow();
    }


    /**
     * @description: 缓存组件执行
     * @author: 田培融
     * @date: 2019/8/6 16:36
      * @param cacheElement 缓存元素
     * @param localOnly false 分布式同步到其他服务中，true  只更新本地服务
     * @return: void
     */
    protected void updateKits(ICacheElement<K, V> cacheElement, boolean localOnly) throws IOException
    {
        if (log.isDebugEnabled())
        {
            if (kitCaches.length > 0)
            {
                log.debug("Update kit caches");
            }
            else
            {
                log.debug("No kit cache to update");
            }
        }

        for (ICache<K, V> kit : kitCaches)
        {
            if (kit == null)
            {
                continue;
            }

            if (log.isDebugEnabled())
            {
                log.debug("Kit cache type: " + kit.getCacheType());
            }

            switch (kit.getCacheType())
            {
                case PAXOS_CACHE:
                    //					if (log.isDebugEnabled())
                    //					{
                    //						log.debug("ce.getElementAttributes().getIsRemote() = "
                    //								+ cacheElement.getElementAttributes().getIsRemote());
                    //					}

                    //if (cacheElement.getElementAttributes().getIsRemote() && !localOnly)
                    if (!localOnly)
                    {
                        try
                        {
                            kit.update(cacheElement);

                            if (log.isDebugEnabled())
                            {
                                log.debug("Update paxos cache  for " + cacheElement.getKey() + cacheElement);
                            }
                        }
                        catch (IOException ex)
                        {
                            log.error("Fail to update in paxos cache", ex);
                        }
                    }
                    break;

                case LATERAL_CACHE:

                    if (log.isDebugEnabled())
                    {
                        log.debug("Lateral cache in kit list: cattr " + cacheAttr.isUseLateral());
                    }

                    if (cacheAttr.isUseLateral() && cacheElement.getElementAttributes().getIsLateral() && !localOnly)
                    {
                        kit.update(cacheElement);

                        if (log.isDebugEnabled())
                        {
                            log.debug("Updated lateral cache for " + cacheElement.getKey());
                        }
                    }
                    break;

                case DISK_CACHE:

                    if (log.isDebugEnabled())
                    {
                        log.debug("Disk cache in kit list: cattr " + cacheAttr.isUseDisk());
                    }

                    if (cacheAttr.isUseDisk() && cacheAttr.getDiskUsagePattern() == DiskUsagePattern.UPDATE
                            && cacheElement.getElementAttributes().getIsSpool())
                    {
                        kit.update(cacheElement);

                        if (log.isDebugEnabled())
                        {
                            log.debug("Updated disk cache for " + cacheElement.getKey());
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    }


    @Override
    public Map<K, ICacheElement<K, V>> getMultiple(Set<K> keys)
    {
        return getMultiple(keys, false);
    }

    protected Map<K, ICacheElement<K, V>> getMultiple(Set<K> keys, boolean localOnly)
    {
        Map<K, ICacheElement<K, V>> elements = new HashMap<K, ICacheElement<K, V>>();

        if (log.isDebugEnabled())
        {
            log.debug("Get: key = " + keys + ", localOnly = " + localOnly);
        }

        try
        {
            Map<K, ICacheElement<K, V>> elementsFromMemory = getMultipleFromMemory(keys);

            elements.putAll(elementsFromMemory);

            if (elements.size() != keys.size())
            {
                Set<K> remainingKeys = pruneKeysFound(keys, elements);

                elements.putAll(getMultipleFromKitCaches(remainingKeys, localOnly));
            }
        }
        catch (IOException e)
        {
            log.error("Get elements error.", e);
        }

        if (elements.size() != keys.size())
        {
            missCountNotFound.addAndGet(keys.size() - elements.size());

            if (log.isDebugEnabled())
            {
                log.debug(cacheAttr.getCacheName() + " - " + (keys.size() - elements.size()) + " not found.");
            }
        }

        return elements;
    }


    private Map<K, ICacheElement<K, V>> getMultipleFromMemory(Set<K> keys) throws IOException
    {
        Map<K, ICacheElement<K, V>> elementsFromMemory = memCache.getMultiple(keys);

        Iterator<ICacheElement<K, V>> iterator = new HashMap<K, ICacheElement<K, V>>(elementsFromMemory).values()
                .iterator();

        while (iterator.hasNext())
        {
            ICacheElement<K, V> element = iterator.next();

            if (element != null)
            {
                if (isExpired(element))
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug(cacheAttr.getCacheName() + " - Memory cache hit, but element expired");
                    }

                    doExpires(element);
                    elementsFromMemory.remove(element.getKey());
                }
                else
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug(cacheAttr.getCacheName() + " - Memory cache hit");
                    }

                    hitCountMemCache.incrementAndGet();
                }
            }
        }
        return elementsFromMemory;
    }


    private Set<K> pruneKeysFound(Set<K> keys, Map<K, ICacheElement<K, V>> foundElements)
    {
        Set<K> remainingKeys = new HashSet<K>(keys);

        for (K key : foundElements.keySet())
        {
            remainingKeys.remove(key);
        }

        return remainingKeys;
    }

    private Map<K, ICacheElement<K, V>> getMultipleFromKitCaches(Set<K> keys, boolean localOnly) throws IOException
    {
        Map<K, ICacheElement<K, V>> elements = new HashMap<K, ICacheElement<K, V>>();

        Set<K> remainingKeys = new HashSet<K>(keys);

        for (KitCache<K, V> kitCache : kitCaches)
        {
            if (kitCache != null)
            {
                Map<K, ICacheElement<K, V>> elementsFromKitCache = new HashMap<K, ICacheElement<K, V>>();

                CacheType cacheType = kitCache.getCacheType();

                if (!localOnly || cacheType == CacheType.DISK_CACHE)
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("Get cache element from kit [" + kitCache.getCacheName() + "] which is of type: "
                                + cacheType);
                    }

                    try
                    {
                        elementsFromKitCache.putAll(kitCache.getMultiple(remainingKeys));
                    }
                    catch (IOException e)
                    {
                        log.error("Get from kit error.", e);
                    }
                }

                if (log.isDebugEnabled())
                {
                    log.debug("Get cache elements: " + elementsFromKitCache);
                }

                processRetrievedElements(kitCache, elementsFromKitCache);

                elements.putAll(elementsFromKitCache);

                if (elements.size() == keys.size())
                {
                    break;
                }
                else
                {
                    remainingKeys = pruneKeysFound(keys, elements);
                }
            }
        }

        return elements;
    }

    private void processRetrievedElements(KitCache<K, V> kitCache, Map<K, ICacheElement<K, V>> elementsFromKitCache)
            throws IOException
    {
        Iterator<ICacheElement<K, V>> iterator = new HashMap<K, ICacheElement<K, V>>(elementsFromKitCache).values()
                .iterator();

        while (iterator.hasNext())
        {
            ICacheElement<K, V> element = iterator.next();

            if (element != null)
            {
                if (isExpired(element))
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug(cacheAttr.getCacheName() + " - kit cache[" + kitCache.getCacheName()
                                + "] hit, but element expired.");
                    }

                    doExpires(element);
                    elementsFromKitCache.remove(element.getKey());
                }
                else
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug(cacheAttr.getCacheName() + " - kit cache[" + kitCache.getCacheName() + "] hit");
                    }

                    hitCountKitCache.incrementAndGet();
                    copyKitCacheRetrievedItemToMemory(element);
                }
            }
        }
    }

    @Override
    public ICacheElement<K, V> get(K key)
    {
        return get(key, false);
    }

    protected ICacheElement<K, V> get(K key, boolean localOnly)
    {
        ICacheElement<K, V> element = null;

        boolean found = false;

        if (log.isDebugEnabled())
        {
            log.debug("Get: key = " + key + ", localOnly = " + localOnly);
        }

        synchronized (this)
        {
            try
            {
                element = memCache.get(key);

                if (element != null)
                {
                    if (isExpired(element))
                    {
                        if (log.isDebugEnabled())
                        {
                            log.debug(cacheAttr.getCacheName() + " Memory cache hit, but element expired");
                        }

                        doExpires(element);
                        element = null;
                    }
                    else
                    {
                        if (log.isDebugEnabled())
                        {
                            log.debug(cacheAttr.getCacheName() + "  Memory cache hit");
                        }

                        hitCountMemCache.incrementAndGet();
                    }

                    found = true;
                }
                else
                {
                    for (KitCache<K, V> kitCache : kitCaches)
                    {
                        if (kitCache != null)
                        {
                            CacheType cacheType = kitCache.getCacheType();

                            if (!localOnly || cacheType == CacheType.DISK_CACHE)
                            {
                                if (log.isDebugEnabled())
                                {
                                    log.debug("Get value from kit  [" + kitCache.getCacheName()
                                            + "] kit cache type is:  " + cacheType);
                                }

                                try
                                {
                                    element = kitCache.get(key);
                                }
                                catch (IOException e)
                                {

                                }
                            }

                            if (log.isDebugEnabled())
                            {
                                log.debug("Get cache element: " + element);
                            }

                            if (element != null)
                            {
                                if (isExpired(element))
                                {
                                    if (log.isDebugEnabled())
                                    {
                                        log.debug(cacheAttr.getCacheName() + " -  kit cache[" + kitCache.getCacheName()
                                                + "] hit, but element expired.");
                                    }

                                    doExpires(element);
                                    element = null;
                                }
                                else
                                {
                                    if (log.isDebugEnabled())
                                    {
                                        log.debug(cacheAttr.getCacheName() + " - kit cache[" + kitCache.getCacheName()
                                                + "] hit");
                                    }

                                    hitCountKitCache.incrementAndGet();
                                    copyKitCacheRetrievedItemToMemory(element);
                                }

                                found = true;

                                break;
                            }
                        }
                    }
                }
            }
            catch (IOException e)
            {
                log.error("Get element error.", e);
            }
        }

        if (!found)
        {
            missCountNotFound.incrementAndGet();

            if (log.isDebugEnabled())
            {
                log.debug(cacheAttr.getCacheName() + " - not find");
            }
        }

        if (element != null)
        {
            element.getElementAttributes().setLastAccessTimeNow();
        }

        return element;
    }


    protected void doExpires(ICacheElement<K, V> element)
    {
        missCountExpired.incrementAndGet();
        remove(element.getKey());
    }

    private void copyKitCacheRetrievedItemToMemory(ICacheElement<K, V> element) throws IOException
    {
        if (memCache.getCacheAttributes().getMaxObjects() > 0)
        {
            memCache.update(element);
        }
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug("No items are allowed to copy in mem memory");
            }
        }
    }


    @Override
    public Map<K, ICacheElement<K, V>> getMatching(String pattern)
    {
        return getMatching(pattern, false);
    }

    public Map<K, ICacheElement<K, V>> localGetMatching(String pattern)
    {
        return getMatching(pattern, true);
    }

    protected Map<K, ICacheElement<K, V>> getMatching(String pattern, boolean localOnly)
    {
        Map<K, ICacheElement<K, V>> elements = new HashMap<K, ICacheElement<K, V>>();

        if (log.isDebugEnabled())
        {
            log.debug("Get: pattern [" + pattern + "], localOnly = " + localOnly);
        }

        try
        {
            elements.putAll(getMatchingFromKitCaches(pattern, localOnly));

            elements.putAll(getMatchingFromMemory(pattern));
        }
        catch (Exception e)
        {
            log.error("Get cache elements error.", e);
        }

        return elements;
    }


    private Map<K, ICacheElement<K, V>> getMatchingFromKitCaches(String pattern, boolean localOnly) throws IOException
    {
        Map<K, ICacheElement<K, V>> elements = new HashMap<K, ICacheElement<K, V>>();

        for (int i = kitCaches.length - 1; i >= 0; i--)
        {
            KitCache<K, V> kitCache = kitCaches[i];

            if (kitCache != null)
            {
                Map<K, ICacheElement<K, V>> elementsFromKitCache = new HashMap<K, ICacheElement<K, V>>();

                CacheType cacheType = kitCache.getCacheType();

                if (!localOnly || cacheType == CacheType.DISK_CACHE)
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("Get pattern from kit [" + kitCache.getCacheName() + "] which is of type: "
                                + cacheType);
                    }

                    try
                    {
                        elementsFromKitCache.putAll(kitCache.getMatching(pattern));
                    }
                    catch (IOException e)
                    {
                        log.error("Error occur in getting from kit", e);
                    }

                    if (log.isDebugEnabled())
                    {
                        log.debug("Get CacheElements: " + elementsFromKitCache);
                    }

                    processRetrievedElements(kitCache, elementsFromKitCache);

                    elements.putAll(elementsFromKitCache);
                }
            }
        }

        return elements;
    }

    protected Map<K, ICacheElement<K, V>> getMatchingFromMemory(String pattern) throws IOException
    {
        Set<K> keyArray = memCache.getKeySet();

        Set<K> matchingKeys = getKeyMatcher().getMatchingKeysFromArray(pattern, keyArray);

        return getMultipleFromMemory(matchingKeys);
    }

    public IKeyMatcher<K> getKeyMatcher()
    {
        return this.keyMatcher;
    }


    @Override
    public void dispose()
    {
        dispose(false);
    }

    public void dispose(boolean fromRemote)
    {
        if (alive.compareAndSet(true, false) == false)
        {
            return;
        }

        if (log.isInfoEnabled())
        {
            log.info("In dispose, [" + this.cacheAttr.getCacheName() + "] fromRemote [" + fromRemote + "]");
        }

        synchronized (this)
        {
            if (cacheManager != null)
            {
                cacheManager.freeCache(getCacheName(), fromRemote);
            }

            if (future != null)
            {
                future.cancel(true);
            }

            if (elementEventQueue != null)
            {
                elementEventQueue.dispose();
                elementEventQueue = null;
            }

            for (ICache<K, V> kit : kitCaches)
            {
                try
                {
                    if (kit == null || kit.getStatus() != CacheStatus.ALIVE)
                    {
                        continue;
                    }

                    if (log.isInfoEnabled())
                    {
                        log.info("In dispose, [" + cacheAttr.getCacheName() + "] kit [" + kit.getCacheName() + "]");
                    }

                    if (kit.getCacheType() == CacheType.DISK_CACHE)
                    {
                        int numToFree = memCache.getSize();

                        memCache.freeElements(numToFree);

                        if (log.isInfoEnabled())
                        {
                            log.info("In dispose, [" + cacheAttr.getCacheName() + "] put " + numToFree + " into kit "
                                    + kit.getCacheName());
                        }
                    }

                    kit.dispose();
                }
                catch (IOException ex)
                {
                    log.error("Dispose kit cache error.", ex);
                }
            }

            if (log.isInfoEnabled())
            {
                log.info("In dispose, [" + cacheAttr.getCacheName() + "] disposing of memory cache.");
            }
            try
            {
                memCache.dispose();
            }
            catch (IOException ex)
            {
                log.error("Dispose memCache error.", ex);
            }
        }
    }


    @Override
    public int getSize()
    {
        return memCache.getSize();
    }

    @Override
    public CacheStatus getStatus()
    {
        return alive.get() ? CacheStatus.ALIVE : CacheStatus.DISPOSED;
    }

    @Override
    public String getStats()
    {
        return getStatistics().toString();
    }

    @Override
    public void setKeyMatcher(IKeyMatcher<K> keyMatcher)
    {
        if (keyMatcher != null)
        {
            this.keyMatcher = keyMatcher;
        }
    }

    public void localUpdate(ICacheElement<K, V> ce) throws IOException
    {
        update(ce, true);
    }

    public boolean localRemove(K key)
    {
        return remove(key, true);
    }

    public ICacheElement<K, V> localGet(K key)
    {
        return get(key, true);
    }

    public Set<K> getKeySet()
    {
        return getKeySet(false);
    }

    public Set<K> getKeySet(boolean localOnly)
    {
        HashSet<K> allKeys = new HashSet<K>();

        allKeys.addAll(memCache.getKeySet());
        for (KitCache<K, V> kitCache : kitCaches)
        {
            if (kitCache != null)
            {
                if (!localOnly || kitCache.getCacheType() == CacheType.DISK_CACHE)
                {
                    try
                    {
                        allKeys.addAll(kitCache.getKeySet());
                    }
                    catch (IOException e)
                    {

                    }
                }
            }
        }
        return allKeys;
    }

    public void localRemoveAll() throws IOException
    {
        removeAll(true);
    }

    @Override
    public boolean remove(K key)
    {
        return remove(key, false);
    }


    protected boolean remove(K key, boolean localOnly)
    {
        removeCount.incrementAndGet();

        boolean removed = false;

        synchronized (this)
        {
            try
            {
                removed = memCache.remove(key);
            }
            catch (IOException e)
            {
                log.error(e);
            }

            for (ICache<K, V> kitCache : kitCaches)
            {
                if (kitCache == null)
                {
                    continue;
                }

                CacheType cacheType = kitCache.getCacheType();

                if (localOnly && (cacheType == CacheType.PAXOS_CACHE || cacheType == CacheType.LATERAL_CACHE))
                {
                    continue;
                }
                try
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("Remove " + key + " from cacheType" + cacheType);
                    }

                    boolean b = kitCache.remove(key);

                    if (!removed)
                    {
                        removed = b;
                    }
                }
                catch (IOException ex)
                {
                    log.error("Fail to remove from kit cache", ex);
                }
            }
        }

        return removed;
    }

    @Override
    public void removeAll() throws IOException
    {
        removeAll(false);
    }

    protected void removeAll(boolean localOnly) throws IOException
    {
        synchronized (this)
        {
            try
            {
                memCache.removeAll();

                if (log.isDebugEnabled())
                {
                    log.debug("Remove all keys from the memory cache.");
                }
            }
            catch (IOException ex)
            {
                log.error("Remove all keys error.", ex);
            }

            for (ICache<K, V> kitCache : kitCaches)
            {
                if (kitCache != null && (kitCache.getCacheType() == CacheType.DISK_CACHE || !localOnly))
                {
                    try
                    {
                        if (log.isDebugEnabled())
                        {
                            log.debug("Remove all keys from cacheType" + kitCache.getCacheType());
                        }

                        kitCache.removeAll();
                    }
                    catch (IOException ex)
                    {
                        log.error("Remove all from kit error", ex);
                    }
                }
            }
        }
    }



    @Override
    public String getCacheName() {
        return cacheAttr.getCacheName();
    }

    public IMemoryCache<K, V> getMemoryCache()
    {
        return memCache;
    }

    @Override
    public CacheType getCacheType()
    {
        return CacheType.CACHE_HUB;
    }

    public void setContextCacheManager(ContextCacheManager manager)
    {
        this.cacheManager = manager;
    }

    public void setElementEventQueue(IElementEventQueue queue)
    {
        this.elementEventQueue = queue;
    }

    public void setKitCaches(KitCache<K, V>[] kitCaches)
    {
        this.kitCaches = kitCaches;
    }
}

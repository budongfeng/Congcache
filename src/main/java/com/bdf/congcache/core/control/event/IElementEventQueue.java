package com.bdf.congcache.core.control.event;

import java.io.IOException;

/**
 * @Author 田培融
 * @Description 事物处理器，事件队列
 * @Date 15:01 2019/7/10
 * @Param key事件处理器 value事件
 **/
public interface IElementEventQueue {
    /**
     * @Author 田培融
     * @Description 添加队列
     * @Date 15:15 2019/7/10
     * @Param [handler, event] handler: 事件执行器， event事件
     * @return void
     **/
    <T> void addElementEvent(IElementEventHandler handler,IElementEvent<T> event) throws IOException;

    // 处理
    void dispose();
}

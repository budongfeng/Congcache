package com.bdf.congcache.core.model;


import com.bdf.congcache.core.control.ContextCache;
import com.bdf.congcache.kits.KitCache;

import java.util.Properties;

public interface IContextCacheManager extends IShutdownObservable
{
	<K, V> ContextCache<K, V> getCache(String cacheName);

	<K, V> KitCache<K, V> getKitCache(String kitName, String cacheName);

	Properties getConfigurationProperties();

	String getStats();
}

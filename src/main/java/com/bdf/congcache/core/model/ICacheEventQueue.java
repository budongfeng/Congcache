package com.bdf.congcache.core.model;


import com.bdf.congcache.core.status.IStats;

import java.io.IOException;

public interface ICacheEventQueue<K, V>
{
	enum QueueType
	{
		SINGLE,

		POOLED
	}

	QueueType getQueueType();

	void addPutEvent(ICacheElement<K, V> ce) throws IOException;

	void addRemoveEvent(K key) throws IOException;

	void addRemoveAllEvent() throws IOException;

	void addDisposeEvent() throws IOException;

	long getListenerId();

	void destroy();

	boolean isAlive();

	boolean isWorking();

	int size();

	boolean isEmpty();

	IStats getStatistics();
}

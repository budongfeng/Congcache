package com.bdf.congcache.core.model;


import com.bdf.congcache.core.control.event.IElementEventHandler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author 田培融
 * @Description 缓存内容的属性
 * @Date 9:07 2019/7/11
 **/
public interface IElementAttributes extends Serializable, Cloneable
{
	//元素生命周期
	void setMaxLife(long mls);

	long getMaxLife();

	//元素空闲时间
	void setIdleTime(long idle);

	long getIdleTime();

	//元素个数
	void setSize(int size);

	int getSize();

	long getCreateTime();

	//最近访问时间
	long getLastAccessTime();

	void setLastAccessTimeNow();

	long getTimeToLiveSeconds();

	//是否刷盘
	boolean getIsSpool();

	void setIsSpool(boolean val);

	//是否线性
	boolean getIsLateral();

	void setIsLateral(boolean val);

	//是否主从
	boolean getIsRemote();

	void setIsRemote(boolean val);

	//是否永生
	boolean getIsEternal();

	void setIsEternal(boolean val);


	long getTimeFactorForMilliseconds();

	void setTimeFactorForMilliseconds(long factor);

	IElementAttributes clone();

	ArrayList<IElementEventHandler> getElementEventHandlers();

	//处理器
	void addElementEventHandler(IElementEventHandler eventHandler);

	void addElementEventHandlers(List<IElementEventHandler> eventHandlers);
}

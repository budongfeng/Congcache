package com.bdf.congcache.core.model;

public interface ICacheType {

    enum CacheType{


        CACHE_HUB,

//      本地磁盘缓存插件
        DISK_CACHE,

//      向外广告使用的缓存插件
        LATERAL_CACHE,

        REMOTE_CACHE,

        //用于实现分布式的缓存插件
        PAXOS_CACHE
    }

   CacheType getCacheType();
}

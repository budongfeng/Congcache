package com.bdf.congcache.core.status;

import java.io.Serializable;
import java.util.List;

/**
 * @Author 田培融
 * @Description 缓存使用状态，用于统计
 * @Date 11:09 2019/7/12
 **/
public interface IStats extends Serializable {

    List<IStatElement<?>> getStatElements();

    void setStatElements(List<IStatElement<?>> stats);

    String getTypeName();

    void setTypeName(String name);
}

package com.bdf.congcache.core;

import com.bdf.congcache.core.model.ICache;
import com.bdf.congcache.core.model.ICacheElement;
import com.bdf.congcache.core.model.ICacheListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;

public class CacheKitWrapper<K, V> implements ICacheListener<K, V>
{
	private static final Log log = LogFactory.getLog(CacheKitWrapper.class);

	private final ICache<K, V> cache;

	private long listenerId = 0;

	@Override
	public void setListenerId(long id) throws IOException
	{
		this.listenerId = id;
		log.debug("listenerId = " + id);
	}

	@Override
	public long getListenerId() throws IOException
	{
		return this.listenerId;
	}

	public CacheKitWrapper(ICache<K, V> cache)
	{
		this.cache = cache;
	}

	@Override
	public void handlePut(ICacheElement<K, V> item) throws IOException
	{
		try
		{
			cache.update(item);
		}
		catch (Exception e)
		{

		}
	}

	@Override
	public void handleRemove(String cacheName, K key) throws IOException
	{
		cache.remove(key);
	}

	@Override
	public void handleRemoveAll(String cacheName) throws IOException
	{
		cache.removeAll();
	}

	@Override
	public void handleDispose(String cacheName) throws IOException
	{
		cache.dispose();
	}
}

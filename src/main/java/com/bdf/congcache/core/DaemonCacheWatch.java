package com.bdf.congcache.core;


import com.bdf.congcache.core.model.ICacheListener;
import com.bdf.congcache.core.model.ICacheObserver;
import com.bdf.congcache.core.model.IDaemon;

public class DaemonCacheWatch implements ICacheObserver, IDaemon
{

	@Override
	public <K, V> void addCacheListener(String cacheName, ICacheListener<K, V> obj)
	{

	}

	@Override
	public <K, V> void addCacheListener(ICacheListener<K, V> obj)
	{

	}

	@Override
	public <K, V> void removeCacheListener(String cacheName, ICacheListener<K, V> obj)
	{

	}

	@Override
	public <K, V> void removeCacheListener(ICacheListener<K, V> obj)
	{

	}
}

package com.bdf.congcache.access;


import com.bdf.congcache.access.exception.CacheException;
import com.bdf.congcache.core.status.ICacheStats;

public interface ICongCacheAccessManagement {

    void dispose();

    /**
     * @description: 清空缓存
     * @author: 田培融
     * @date: 2019/8/9 15:26
      * @param
     * @return: void
     */
    void clear() throws CacheException;


    /**
     * @description:  获取缓存上下的属性
     * @author: 田培融
     * @date: 2019/8/9 16:40
      * @param
     * @return: com.bdf.congcache.core.status.ICacheStats
     */
    ICacheStats getStatistics();

    String getStats();
}

package com.bdf.congcache.access;

import com.bdf.congcache.access.exception.CacheException;
import com.bdf.congcache.access.exception.InvalidArgumentException;
import com.bdf.congcache.access.exception.InvalidHandleException;
import com.bdf.congcache.access.exception.ObjectExistsException;
import com.bdf.congcache.core.CacheElement;
import com.bdf.congcache.core.control.ContextCache;
import com.bdf.congcache.core.model.ICacheElement;
import com.bdf.congcache.core.model.IElementAttributes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author 田培融
 * @Description 保存了ContextCache
 * @Date 18:59 2019/7/10
 **/
public class CongCacheAccess<K,V> extends AbstractCongCacheAccess<K,V> implements ICongCacheAccess<K,V>{

    private static final Log log = LogFactory.getLog(CongCacheAccess.class);


    public CongCacheAccess(ContextCache<K, V> cacheControl)
    {
        super(cacheControl);
    }

    /**
     * @description: 获取缓存元素
     * @author: 田培融
     * @date: 2019/7/26 7:46
      * @param name	缓存K
     * @return: V
     */
    @Override
    public V get(K name)
    {
        ICacheElement<K, V> element = this.getCacheControl().get(name);

        return (element != null) ? element.getVal() : null;
    }

    @Override
    public Map<K, V> getMatching(String pattern)
    {
        HashMap<K, V> unwrappedResults = new HashMap<K, V>();

        Map<K, ICacheElement<K, V>> wrappedResults = this.getCacheControl().getMatching(pattern);
        if (wrappedResults != null)
        {
            for (Map.Entry<K, ICacheElement<K, V>> entry : wrappedResults.entrySet())
            {
                ICacheElement<K, V> element = entry.getValue();
                if (element != null)
                {
                    unwrappedResults.put(entry.getKey(), element.getVal());
                }
            }
        }
        return unwrappedResults;
    }

    @Override
    public ICacheElement<K, V> getCacheElement(K name)
    {
        return this.getCacheControl().get(name);
    }

    @Override
    public Map<K, ICacheElement<K, V>> getCacheElements(Set<K> names)
    {
        return this.getCacheControl().getMultiple(names);
    }

    @Override
    public Map<K, ICacheElement<K, V>> getMatchingCacheElements(String pattern)
    {
        return this.getCacheControl().getMatching(pattern);
    }

    @Override
    public void putSafe(K key, V value)
    {
        if (this.getCacheControl().get(key) != null)
        {
            throw new ObjectExistsException("putSafe failed.  Object exists in the cache for key [" + key + "]");
        }
        put(key, value);
    }


    /**
     * @description: 添加缓存
     * @author: 田培融
     * @date: 2019/7/31 8:32
      * @param name	缓存K
     * @param obj   缓存Value，可以是任意值
     * @return: void
     */
    @Override
    public void put(K name, V obj)
    {

        put(name, obj, this.getCacheControl().getElementAttributes());
    }

    /**
     * @description: 添加缓存，并给缓存添加属性
     * @author: 田培融
     * @date: 2019/7/31 8:34
      * @param key	缓存k
     * @param val	缓存V
     * @param attr	缓存属性
     * @return: void
     */
    @Override
    public void put(K key, V val, IElementAttributes attr)
    {
        if (key == null)
        {
            throw new InvalidArgumentException("Key must not be null");
        }

        if (val == null)
        {
            throw new InvalidArgumentException("Value must not be null");
        }

        try
        {
            // 创建缓存元素
            CacheElement<K, V> ce = new CacheElement<K, V>(this.getCacheControl().getCacheName(), key, val);

            ce.setElementAttributes(attr);

            // 缓存上下文件添加缓存元素
            this.getCacheControl().update(ce);
        }
        catch (IOException e)
        {
            throw new CacheException(e);
        }
    }

    @Override
    public void remove(K name)
    {
        this.getCacheControl().remove(name);
    }

    @Override
    public void resetElementAttributes(K name, IElementAttributes attr)
    {
        ICacheElement<K, V> element = this.getCacheControl().get(name);

        if (element == null)
        {
            throw new InvalidHandleException("Object for name [" + name + "] is not in the cache");
        }

        put(element.getKey(), element.getVal(), attr);
    }

    @Override
    public IElementAttributes getElementAttributes(K name)
    {
        IElementAttributes attr = null;

        try
        {
            attr = this.getCacheControl().getElementAttributes(name);
        }
        catch (IOException ioe)
        {
            log.error("Fail to  get element attributes", ioe);
        }

        return attr;
    }





}

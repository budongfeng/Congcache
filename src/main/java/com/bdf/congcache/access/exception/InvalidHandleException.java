package com.bdf.congcache.access.exception;

public class InvalidHandleException extends CacheException
{

	private static final long serialVersionUID = 1L;

	public InvalidHandleException()
	{
		super();
	}

	public InvalidHandleException(String message)
	{
		super(message);
	}

}

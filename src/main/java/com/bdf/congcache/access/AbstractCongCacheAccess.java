package com.bdf.congcache.access;

import com.bdf.congcache.access.exception.CacheException;
import com.bdf.congcache.core.control.ContextCache;
import com.bdf.congcache.core.status.ICacheStats;

import java.io.IOException;

/**
 * @Author 田培融
 * @Description 使用抽像类可以避免使用此类创建对象
 * @Date 19:05 2019/7/10
 **/
public class AbstractCongCacheAccess<K,V> implements ICongCacheAccessManagement {

    private final ContextCache<K, V> cacheControl;

    protected AbstractCongCacheAccess(ContextCache<K, V> cacheControl)
    {
        this.cacheControl = cacheControl;
    }

    public ContextCache<K, V> getCacheControl()
    {
        return cacheControl;
    }

    @Override
    public void clear() throws CacheException
    {
        try
        {
            this.getCacheControl().removeAll();
        }
        catch (IOException e)
        {
            throw new CacheException(e);
        }
    }


    @Override
    public void dispose()
    {
        this.getCacheControl().dispose();
    }

    @Override
    public ICacheStats getStatistics()
    {
        return this.getCacheControl().getStatistics();
    }

    @Override
    public String getStats()
    {
        return this.getCacheControl().getStats();
    }
}